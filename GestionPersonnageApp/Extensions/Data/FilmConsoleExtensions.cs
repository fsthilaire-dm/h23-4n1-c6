﻿namespace GestionPersonnageApp.Extensions.Data;

/// <summary>
/// Classe statique qui regroupe les méthodes d'extension pour la console du modèle Film
/// </summary>
public static class FilmConsoleExtensions
{
    private const string PAS_TROUVE_PAR_CLE = "Film non trouvé.";

    /// <summary>
    /// Méthode qui affiche l'information d'un film à la console
    /// </summary>
    /// <param name="film">Film</param>
    public static void AfficherConsole(this Film? film)
    {
        if (film != null)
        {
            Console.WriteLine($"Id : {film.FilmId}");
            Console.WriteLine($"Titre : {film.Titre}");
            Console.WriteLine($"Durée : {film.Duree} minute(s)");
            Console.WriteLine($"Étoile : {film.Etoile}");
            Console.WriteLine($"Budget : {film.Budget:N2}");
            Console.WriteLine($"Date Sortie : {film.DateSortie:d MMMM yyyy}");
        }
        else
        {
            Console.WriteLine(PAS_TROUVE_PAR_CLE);
        }
    }

    /// <summary>
    /// Méthode qui affiche l'information d'une liste de films à la console
    /// </summary>
    /// <param name="lstFilm"></param>
    public static void AfficherConsole(this List<Film> lstFilm)
    {
        if (lstFilm?.Count > 0)
        {
            foreach (Film film in lstFilm)
            {
                film.AfficherConsole();
                Console.WriteLine(Environment.NewLine);
            }
        }
        else
        {
            Console.WriteLine("Il n'y a aucun film dans la base de données.");
        }
    }

    public static void AfficherConsoleFilmDetail(this Film? film)
    {
        if (film != null) 
        {
            Console.WriteLine($"Id : {film.FilmId}");
            Console.WriteLine($"Titre : {film.Titre}");
            Console.WriteLine(Environment.NewLine);
            Console.WriteLine("Distribution");

            foreach(Distribution distribution in film.Distribution)
            {
                string lignePersonnage;

                //Vérifie s'il y a une identité réelle pour ce personnage
                if(string.IsNullOrWhiteSpace(distribution.Personnage?.IdentiteReelle) == false)
                {
                    //Il y a une identité réelle, son identité est affichée
                    lignePersonnage = $"\t{distribution.Personnage?.Nom} / {distribution.Personnage?.IdentiteReelle}".PadRight(50);
                }
                else
                {
                    //Il n'y a pas d'identité réelle, seulement son nom qui est affiché
                    lignePersonnage = $"\t{distribution.Personnage?.Nom}".PadRight(50);
                }

                lignePersonnage += $" interprété(e) par {distribution.Acteur}";

                Console.WriteLine(lignePersonnage);
            }
        }
        else
        {
            Console.WriteLine(PAS_TROUVE_PAR_CLE);
        }
    }
}
