﻿namespace GestionPersonnageApp.Extensions.Data;

/// <summary>
/// Classe statique qui regroupe les méthodes d'extension pour la console du modèle Personnage
/// </summary>
public static class PersonnageConsoleExtensions
{
    /// <summary>
    /// Méthode qui affiche l'information d'un personnage à la console
    /// </summary>
    /// <param name="personnage">Personnage</param>
    public static void AfficherConsole(this Personnage? personnage)
    {
        if (personnage != null)
        {
            Console.WriteLine($"Id : {personnage.PersonnageId}");
            Console.WriteLine($"Nom : {personnage.Nom}");

            //Le ?? permet d'indiquer la valeur de remplacement si IdentiteReelle est null
            Console.WriteLine($"Identité réelle : {personnage.IdentiteReelle ?? "Inconnue"}");

            //Affiche la date de naissance en d MMMM yyyy -> 3 décembre 1998
            Console.Write("Date de naissance : ");

            if (personnage.DateNaissance != null)
            {
                Console.WriteLine($"{personnage.DateNaissance:d MMMM yyyy}");
            }
            else
            {
                Console.WriteLine("Inconnue");
            }

            Console.WriteLine($"Est vilain : {(personnage.EstVilain ? "Oui" : "Non")}");

            Console.WriteLine($"Univers Id : {personnage.UniversId}");

            if (personnage.Univers != null)
            {
                Console.WriteLine($"Nom univers : {personnage.Univers.Nom}");
            }
        }
        else
        {
            Console.WriteLine("Personnage non trouvé.");
        }
    }

    /// <summary>
    /// Méthode qui affiche l'information d'une liste de personnages à la console
    /// </summary>
    /// <param name="lstPersonnage">Liste de personnages</param>
    public static void AfficherConsole(this List<Personnage> lstPersonnage)
    {
        if (lstPersonnage?.Count > 0)
        {
            foreach (Personnage personnage in lstPersonnage)
            {
                personnage.AfficherConsole();
                Console.WriteLine(Environment.NewLine);
            }
        }
        else
        {
            Console.WriteLine("Il n'y a aucun personnage dans la base de données.");
        }
    }

    /// <summary>
    /// Méthode qui affiche l'information d'une liste de PersonnageStatFilms à la console
    /// </summary>
    /// <param name="lstPersonnageStatFilms">Liste de PersonnageStatFilms</param>
    public static void AfficherConsole(this List<PersonnageStatFilms> lstPersonnageStatFilms)
    {
        //Création de l'entête
        Console.WriteLine("Univers".PadRight(20) +
                          "Personnage".PadRight(30) +
                          "Budget moyen".PadRight(15) +
                          "Nombre de films".PadRight(18) + 
                          "Durée totale");

        Console.WriteLine("-------".PadRight(20) +
                          "----------".PadRight(30) +
                          "------------".PadRight(15) +
                          "---------------".PadRight(18) +
                          "------------");

        foreach(PersonnageStatFilms item in lstPersonnageStatFilms)
        {
            Console.WriteLine(item.UniversNom.PadRight(20) +
                              item.PersonnageNom.PadRight(30) +
                              item.BudgetMoyen.ToString().PadRight(15) +
                              item.NbFilm.ToString().PadRight(18) +
                              item.DureeTotale.ToString());
        }
    }
}
