﻿using Microsoft.Extensions.DependencyInjection;

namespace GestionPersonnageApp.Extensions.ServiceCollections;

/// <summary>
/// Classe d'extension qui permet d'enregistrer les classes de la catégorie Repository
/// </summary>
public static class SCRepositoryExtensions
{
    /// <summary>
    /// Méthode qui permet d'enregistrer les repositories de l'application
    /// </summary>
    /// <param name="services">La collection de services</param>
    public static void EnregistrerRepositories(this IServiceCollection services)
    {
        services.AddTransient<IUniversRepo, UniversRepo>();
        services.AddTransient<IFilmRepo, FilmRepo>();
        services.AddTransient<IPersonnageRepo, PersonnageRepo>();
    }
}
