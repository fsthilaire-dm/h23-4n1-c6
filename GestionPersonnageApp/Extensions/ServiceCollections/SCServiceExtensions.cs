﻿using Microsoft.Extensions.DependencyInjection;

namespace GestionPersonnageApp.Extensions.ServiceCollections;

/// <summary>
/// Classe d'extension qui permet d'enregistrer les classes de la catégorie Service
/// </summary>
public static class SCServiceExtensions
{
    /// <summary>
    /// Méthode qui permet d'enregistrer les services de l'application
    /// </summary>
    /// <param name="services">La collection de services</param>
    public static void EnregistrerServices(this IServiceCollection services)
    {
        services.AddTransient<IUniversService, UniversService>();
        services.AddTransient<IFilmService, FilmService>();
        services.AddTransient<IPersonnageService, PersonnageService>();
    }
}
