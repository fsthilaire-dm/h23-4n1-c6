﻿namespace GestionPersonnageApp.Repositories;

/// <summary>
/// Interface qui contient les méthodes de communication avec la base de données pour la table Personnage
/// </summary>
public interface IPersonnageRepo
{
    /// <summary>
    /// Obtenir un personnage à partir de sa clé primaire
    /// </summary>
    /// <param name="personnageId">Clé primaire</param>
    /// <returns>Le personnage s'il existe, sinon null si non trouvé</returns>    
    Personnage? ObtenirPersonnage(int personnageId);

    /// <summary>
    /// Obtenir un personnage à partir de sa clé primaire
    /// </summary>
    /// <param name="PersonnageId">Clé primaire</param>
    /// <param name="inclureUnivers">Inclure l'univers ou non dans le modèle</param>
    /// <returns>Le personnage s'il existe, sinon null si non trouvé</returns>
    Personnage? ObtenirPersonnage(int personnageId, bool inclureUnivers);

    /// <summary>
    /// Obtenir la liste de tous les personnages
    /// </summary>
    /// <returns>Liste des personnages de la base de données</returns>
    List<Personnage> ObtenirListe();

    /// <summary>
    /// Obtenir la liste de tous les personnages
    /// </summary>
    /// <param name="inclureUnivers">Inclure l'univers ou non dans le modèle</param>
    /// <returns>Liste des personnages de la base de données</returns>
    List<Personnage> ObtenirListe(bool inclureUnivers);

    /// <summary>
    /// Obtenir le rapport des statistiques des films pour les personnages d'un univers précis
    /// </summary>
    /// <param name="universId">La clé de l'univers des personnages</param>
    /// <param name="utiliserVue">Utiliser la Vue SQL ou en pur LINQ</param>
    /// <returns>Liste des statistiques par personnage</returns>
    List<PersonnageStatFilms> ObtenirRapportPersonnageStatFilms(int universId, bool utiliserVue);
}