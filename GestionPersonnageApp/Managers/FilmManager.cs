﻿namespace GestionPersonnageApp.Managers;

/// <summary>
/// Classe qui s'occupe de la coordination de modèle Film
/// </summary>
public class FilmManager : IFilmManager
{
    private readonly IFilmService _filmService;

    public FilmManager(IFilmService filmService)
    {
        _filmService = filmService;
    }

    public void AfficherListe()
    {
        //Utilisation directe de l'affichage
        _filmService.ObtenirListe().AfficherConsole();
    }

    public void AfficherParId()
    {

        /*Code à remplacer pour utiliser une classe utilitaire*/
        int filmId;
        bool valide;
        do
        {
            Console.WriteLine("Entrer la clé du film.");

            valide = Int32.TryParse(Console.ReadLine(), out filmId);

            if (valide == false)
            {
                Console.WriteLine("Ce n'est pas un nombre. Essayez de nouveau.");
            }

        } while (valide == false);
        /*Fin du code à remplacer*/

        //Utilisation directe de l'affichage
        _filmService.ObtenirFilm(filmId).AfficherConsole();
    }

    public void AfficherFilmDetailParId()
    {
        /*Code à remplacer pour utiliser une classe utilitaire*/
        int filmId;
        bool valide;
        do
        {
            Console.WriteLine("Entrer la clé du film.");

            valide = Int32.TryParse(Console.ReadLine(), out filmId);

            if (valide == false)
            {
                Console.WriteLine("Ce n'est pas un nombre. Essayez de nouveau.");
            }

        } while (valide == false);
        /*Fin du code à remplacer*/

        //Utilisation directe de l'affichage
        _filmService.ObtenirFilmDetail(filmId).AfficherConsoleFilmDetail();
    }
}