﻿namespace GestionPersonnageApp.Managers;

/// <summary>
/// Classe qui s'occupe de la coordination de modèle Univers
/// </summary>
public class UniversManager : IUniversManager
{
    private readonly IUniversService _universService;

    public UniversManager(IUniversService universService)
    {
        _universService = universService;
    }

    public void AfficherListe()
    {
        //Utilisation directe de l'affichage
        _universService.ObtenirListe().AfficherConsole();
    }

    public void AfficherParId()
    {

        /*Code à remplacer pour utiliser une classe utilitaire*/
        int universId;
        bool valide;
        do
        {
            Console.WriteLine("Entrer la clé de l'univers.");
            
            valide = Int32.TryParse(Console.ReadLine(), out universId);

            if(valide == false)
            {
                Console.WriteLine("Ce n'est pas un nombre. Essayez de nouveau.");
            }

        } while (valide == false);
        /*Fin du code à remplacer*/

        //Utilisation directe de l'affichage
        _universService.ObtenirUnivers(universId).AfficherConsole();
    }

    public void SupprimerParId()
    {
        /*Code à remplacer pour utiliser une classe utilitaire*/
        int universId;
        bool valide;
        do
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Entrer la clé de l'univers.");
            Console.ForegroundColor = ConsoleColor.Gray;

            valide = Int32.TryParse(Console.ReadLine(), out universId);

            if (valide == false)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Ce n'est pas un nombre. Essayez de nouveau.");
                Console.ForegroundColor = ConsoleColor.Gray;
            }

        } while (valide == false);
        /*Fin du code à remplacer*/

        //Utilisation directe de l'affichage


        UniversDependance? universDependance = _universService.ObtenirDependance(universId);

        bool supprimer = universDependance.SupprimerConsole();

        if(supprimer == true)
        {
            _universService.SupprimerUnivers(universDependance!.Univers.UniversId);
        }
    }
}