﻿namespace GestionPersonnageApp.Managers;

/// <summary>
/// Interface qui s'occupe de la coordination du modèle Film
/// </summary>
public interface IFilmManager
{
    /// <summary>
    /// Afficher tous les films
    /// </summary>
    void AfficherListe();

    /// <summary>
    /// Afficher un film en fonction de sa clé primaire
    /// </summary>        
    void AfficherParId();

    /// <summary>
    /// Afficher le détail complet d'un film en fonction de sa clé primaire
    /// </summary>    
    void AfficherFilmDetailParId();
}