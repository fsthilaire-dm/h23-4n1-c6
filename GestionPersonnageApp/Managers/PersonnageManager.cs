﻿namespace GestionPersonnageApp.Managers;

/// <summary>
/// Classe qui s'occupe de la coordination de modèle Personnage
/// </summary>
public class PersonnageManager : IPersonnageManager
{
    private readonly IPersonnageService _personnageService;

    public PersonnageManager(IPersonnageService personnageService)
    {
        _personnageService = personnageService;
    }

    public void AfficherListe()
    {
        /*Code à remplacer pour utiliser une classe utilitaire*/
        //Mettre ce genre de questions dans un utilitaire.
        //Il faut s'assurer que c'est seulement O ou N. 
        //Dans cet exemple, le cas Z sera considéré comme un N

        Console.WriteLine("Il faut afficher le détail des clés étrangères ? [O/N]");
        bool afficherUnivers;

        if (Console.ReadLine()?.ToUpper() == "O")
        {
            afficherUnivers = true;
        }
        else
        {
            afficherUnivers = false;
        }

        /*Fin du code à remplacer*/

        //Utilisation directe de l'affichage
        _personnageService.ObtenirListe(afficherUnivers).AfficherConsole();
    }

    public void AfficherParId()
    {

        /*Code à remplacer pour utiliser une classe utilitaire*/
        int personnageId;
        bool valide;
        do
        {
            Console.WriteLine("Entrer la clé du personnage.");

            valide = Int32.TryParse(Console.ReadLine(), out personnageId);

            if (valide == false)
            {
                Console.WriteLine("Ce n'est pas un nombre. Essayez de nouveau.");
            }

        } while (valide == false);


        //Mettre ce genre de questions dans un utilitaire.
        //Il faut s'assurer que c'est seulement O ou N. 
        //Dans cet exemple, le cas Z sera considéré comme un N

        Console.WriteLine("Il faut afficher le détail des clés étrangères ? [O/N]");
        bool afficherUnivers;

        if (Console.ReadLine()?.ToUpper() == "O")
        {
            afficherUnivers = true;
        }
        else
        {
            afficherUnivers = false;
        }

        /*Fin du code à remplacer*/

        //Utilisation directe de l'affichage
        _personnageService.ObtenirPersonnage(personnageId, afficherUnivers).AfficherConsole();
    }

    public void AfficherRapportPersonnageStatFilms()
    {
        /*Code à remplacer pour utiliser une classe utilitaire*/
        int universId;
        bool valide;
        do
        {
            Console.WriteLine("Entrer la clé de l'univers.");

            valide = Int32.TryParse(Console.ReadLine(), out universId);

            if (valide == false)
            {
                Console.WriteLine("Ce n'est pas un nombre. Essayez de nouveau.");
            }

        } while (valide == false);

        //Mettre ce genre de questions dans un utilitaire.
        //Il faut s'assurer que c'est seulement O ou N. 
        //Dans cet exemple, le cas Z sera considéré comme un N

        Console.WriteLine("Il faut utiliser la vue SQL ? [O/N]");
        bool utiliserVue;

        if (Console.ReadLine()?.ToUpper() == "O")
        {
            utiliserVue = true;
        }
        else
        {
            utiliserVue = false;
        }

        _personnageService.ObtenirRapportPersonnageStatFilms(universId, utiliserVue).AfficherConsole();
    }
}