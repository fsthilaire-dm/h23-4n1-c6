﻿using GestionPersonnageApp.Managers;
using Microsoft.Extensions.DependencyInjection;

namespace GestionPersonnageApp;

/// <summary>
/// Classe qui représente la base de l'application console
/// </summary>
public class App
{
    private readonly IServiceProvider _serviceProvider;
    private readonly string[] _args;

    /// <summary>
    /// Constructeur
    /// </summary>
    /// <param name="serviceProvider">Fournisser de service pour récuppérer les dépendances</param>
    public App(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;

        //Verifie si le programme a des arguments spécifiques
        if (Environment.GetCommandLineArgs().Length > 1)
        {
            //Le programme a des arguments spécifiques
            //Récupère à partir du 2e argument, car le premier est la référence du programme
            _args = Environment.GetCommandLineArgs().ToList().GetRange(1, Environment.GetCommandLineArgs().Length - 1).ToArray();
        }
        else
        {
            //Il n'y a aucun argument spécifique
            //Crée un tableau vide
            _args = new string[] { "-aide" };
        }

    }


    /// <summary>
    /// Méthode qui s'occupe du démarrage de l'application
    /// </summary>
    /// <returns>Tâches</returns>
    public async Task DemarrerApplicationAsync()
    {
        //Le point de départ de la logique de l'application

        switch (_args[0].ToLower())
        {
            case "-aide":
                AfficherAide();
                break;

            case "-univers":

                ModuleUnivers();

                break;

            case "-film":

                ModuleFilm();

                break;

            case "-personnage":

                ModulePersonnage();

                break;

            default:
                Console.WriteLine("Argument non valide");
                break;
        }

        //Nécessaire, car il n'y a aucun await dans le code et la méthode est async
        await Task.CompletedTask;
    }

    /// <summary>
    /// Méthode qui permet de gérer les arguments pour le module univers.
    /// </summary>
    private void ModuleUnivers()
    {
        if (_args.Length == 2)
        {
            IUniversManager universManager = _serviceProvider.GetRequiredService<IUniversManager>();

            switch (_args[1].ToLower())
            {
                case "-afficher":
                    universManager.AfficherParId();
                    break;

                case "-afficherliste":
                    universManager.AfficherListe();
                    break;

                case "-supprimer":
                    universManager.SupprimerParId();
                    break;

                default:
                    AfficherAide(true);
                    break;
            }
        }
        else
        {
            AfficherAide(true);
        }
    }

    /// <summary>
    /// Méthode qui permet de gérer les arguments pour le module film.
    /// </summary>
    private void ModuleFilm()
    {
        if (_args.Length == 2)
        {
            IFilmManager filmManager = _serviceProvider.GetRequiredService<IFilmManager>();

            switch (_args[1].ToLower())
            {
                case "-afficher":
                    filmManager.AfficherParId();
                    break;

                case "-afficherliste":
                    filmManager.AfficherListe();
                    break;

                case "-afficherdetail":
                    filmManager.AfficherFilmDetailParId();
                    break;

                default:
                    AfficherAide(true);
                    break;
            }
        }
        else
        {
            AfficherAide(true);
        }
    }

    /// <summary>
    /// Méthode qui permet de gérer les arguments pour le module personnage.
    /// </summary>
    private void ModulePersonnage()
    {
        if (_args.Length == 2)
        {
            IPersonnageManager personnageManager = _serviceProvider.GetRequiredService<IPersonnageManager>();

            switch (_args[1].ToLower())
            {
                case "-afficher":
                    personnageManager.AfficherParId();
                    break;

                case "-afficherliste":
                    personnageManager.AfficherListe();
                    break;

                case "-rapportpersonnagestatfilms":
                    personnageManager.AfficherRapportPersonnageStatFilms();
                    break;

                default:
                    AfficherAide(true);
                    break;
            }
        }
        else
        {
            AfficherAide(true);
        }
    }

    /// <summary>
    /// Méthode qui affiche le message d'aide du programme
    /// </summary>
    /// <param name="afficherInvalide">Afficher le message d'argument invalide</param>
    private void AfficherAide(bool afficherInvalide = false)
    {
        if (afficherInvalide == true)
        {
            Console.WriteLine("Argument invalide." + Environment.NewLine);
        }

        Console.WriteLine("Voici les paramètres à utiliser.");
        Console.WriteLine("-aide        Affiche l'aide du programme.");
        Console.WriteLine("-[module] -[tâche]       Permet de sélectionner le module et appliquer une tâche.");
        Console.WriteLine("La liste des modules : univers.");
        Console.WriteLine("La liste des tâches : afficher.");
    }
}