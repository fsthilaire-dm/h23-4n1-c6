﻿namespace GestionPersonnageApp.Services;

/// <summary>
/// Classe qui contient les services du modèle Univers
/// </summary>
public class UniversService : IUniversService
{
    private readonly IUniversRepo _universRepo;

    public UniversService(IUniversRepo universRepo)
    {
        _universRepo = universRepo;
    }

    public Univers? ObtenirUnivers(int universId)
    {
        return _universRepo.ObtenirUnivers(universId);
    }

    public List<Univers> ObtenirListe()
    {
        return _universRepo.ObtenirListe();
    }

    public UniversDependance? ObtenirDependance(int universId)
    {
        return _universRepo.ObtenirDependance(universId);
    }

    public void SupprimerUnivers(int universId)
    {
        UniversDependance? universDependance = _universRepo.ObtenirDependance(universId);

        if(universDependance != null)
        {
            if(universDependance.NbPersonnages == 0)
            {
                _universRepo.SupprimerUnivers(universDependance.Univers);
            }
            else
            {
                throw new Exception("L'univers a des dépendances. Impossible de le supprimer.");
            }
        }
        else
        {
            throw new Exception("L'univers n'existe pas dans la base de données.");
        }
    }
}
