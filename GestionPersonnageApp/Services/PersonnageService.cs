﻿namespace GestionPersonnageApp.Services;

/// <summary>
/// Classe qui contient les services du modèle Personnage
/// </summary>
public class PersonnageService : IPersonnageService
{
    private readonly IPersonnageRepo _personnageRepo;

    public PersonnageService(IPersonnageRepo personnageRepo)
    {
        _personnageRepo = personnageRepo;
    }

    public Personnage? ObtenirPersonnage(int personnageId)
    {
        return _personnageRepo.ObtenirPersonnage(personnageId);
    }

    public Personnage? ObtenirPersonnage(int personnageId, bool inclureUnivers)
    {
        return _personnageRepo.ObtenirPersonnage(personnageId, inclureUnivers);
    }

    public List<Personnage> ObtenirListe()
    {
        return _personnageRepo.ObtenirListe();
    }

    public List<Personnage> ObtenirListe(bool inclureUnivers)
    {
        return _personnageRepo.ObtenirListe(inclureUnivers);
    }

    public List<PersonnageStatFilms> ObtenirRapportPersonnageStatFilms(int universId, bool utiliserVue)
    {
        return _personnageRepo.ObtenirRapportPersonnageStatFilms(universId, utiliserVue);
    }
}
