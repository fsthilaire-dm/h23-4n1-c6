﻿namespace GestionPersonnageApp.Data.Rapports;

/// <summary>
/// Classe qui contient les statistiques des films d'un personnage
/// </summary>
public class PersonnageStatFilms
{
    public int PersonnageId { get; set; }
    public string PersonnageNom { get; set; }
    public int UniversId { get; set; }
    public string UniversNom { get; set; }
    public decimal BudgetMoyen { get; set; }
    public int NbFilm { get; set; }
    public int DureeTotale { get; set; }
}
