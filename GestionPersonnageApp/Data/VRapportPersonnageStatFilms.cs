﻿using System;
using System.Collections.Generic;

namespace GestionPersonnageApp.Data;

public partial class VRapportPersonnageStatFilms
{
    public int UniversId { get; set; }

    public string UniversNom { get; set; } = null!;

    public int PersonnageId { get; set; }

    public string PersonnageNom { get; set; } = null!;

    public decimal? BudgetMoyen { get; set; }

    public int? NbFilm { get; set; }

    public int? DureeTotale { get; set; }
}
