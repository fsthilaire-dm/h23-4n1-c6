﻿namespace GestionPersonnageApp.Data.Dependances;

/// <summary>
/// Classe qui contient le nombre de dépendances pour un univers.
/// </summary>
public class UniversDependance
{
    public Univers Univers { get; init; } = null!;
    public int NbPersonnages { get; init; }
}
